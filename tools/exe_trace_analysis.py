# coding=utf-8
import re
import time
from enum import IntEnum
from typing import List, Callable, TypeVar, Iterable, Generator, Optional, Tuple, Iterator, Dict, Set
from pathlib import Path
import subprocess as sp
from string import Template
from dataclasses import dataclass

T = TypeVar( "T" )
Predicat = Callable[ [ T ], bool ]

S = TypeVar( "S" )

timers: Dict[ str, int ] = { }

def select_range( entry_cond: Predicat, exit_cond: Predicat, data: Iterable[ T ] ) \
        -> Generator[ List[ T ], None, None ] :
    res = [ ]
    entry = False
    for d in data :
        if entry :
            res.append( d )
        elif entry_cond( d ) :
            res.append( d )
            entry = True
        if entry and exit_cond( d ) :
            yield [ d for d in res ]
            res = [ ]
            entry = False


@dataclass
class DFILoad( object ) :
    kernel_add: int
    load_add: int
    check_add: int
    check_tag_start_add: Set[int]
    end_add: int

    def __repr__( self ) :
        return "DFILoad(kadd={kadd}, ladd={ladd}, cadd={cadd}, set_adds={set_adds}, eadd={eadd})".format(
            kadd=hex( self.kernel_add ),
            ladd=hex( self.load_add ),
            cadd=hex( self.check_add ),
            set_adds=set(map(hex, self.check_tag_start_add)),
            eadd=hex( self.end_add )
        )


@dataclass
class DFIStore( object ) :
    sandbox_add: Optional[ int ]
    kernel_add: int
    store_add: int
    end_add: int

    def __repr__( self ) :
        return "DFILoad(sadd={sadd}, kadd={kadd}, cadd={cadd}, eadd={eadd})".format(
            sadd=hex( self.sandbox_add ) if self.sandbox_add is not None else "None",
            kadd=hex( self.kernel_add ),
            cadd=hex( self.store_add ),
            eadd=hex( self.end_add )
        )


@dataclass( order=True )
class AsmInst( object ) :
    address: int
    instruction: str
    operands: str

    def __repr__( self ) :
        return f"TracePart(address={hex( self.address )}, instruction={self.instruction}, operands={self.operands})"


@dataclass
class ExecutionStep( object ) :
    pc: int

    def __repr__( self ) :
        return f"ExecutionStep(pc={hex( self.pc )})"


class CostType( IntEnum ) :
    STORE_SANDBOX = 0
    STORE_KERNEL = 1
    STORE_EXEC = 2
    LOAD_KERNEL = 3
    LOAD_EXEC = 4
    LOAD_CHECKS = 5


@dataclass
class ExecutionStats( object ) :
    store_sandbox: int = 0
    store_kernel: int = 0
    store_exec: int = 0
    load_kernel: int = 0
    load_exec: int = 0
    load_checks: int = 0
    check_nb: int = 0
    load_nb: int = 0
    store_nb: int = 0
    branch_info: Optional[ Tuple[ int, CostType ] ] = None

    def store_branch_data( self, address: int, ty: CostType ) :
        self.branch_info = (address, ty)

    def resolve_branch( self, new_address: int ) :
        if self.branch_info is None :
            return

        if new_address != self.branch_info[ 0 ] + 0x4 :
            if self.branch_info[ 1 ] == CostType.STORE_SANDBOX :
                self.store_sandbox += 1
            elif self.branch_info[ 1 ] == CostType.STORE_KERNEL :
                self.store_kernel += 1
            elif self.branch_info[ 1 ] == CostType.STORE_EXEC :
                self.store_exec += 1
            elif self.branch_info[ 1 ] == CostType.LOAD_KERNEL :
                self.load_kernel += 1
            elif self.branch_info[ 1 ] == CostType.LOAD_EXEC :
                self.load_exec += 1
            elif self.branch_info[ 1 ] == CostType.LOAD_CHECKS :
                self.load_checks += 1
            else :
                raise Exception( "Found CostType not in CostType" )

        self.branch_info = None

    @property
    def total_cost( self ) :
        return self.store_sandbox + self.store_kernel + self.store_exec + \
               self.load_kernel + self.load_exec + self.load_checks


def gen_trace( binary_path: Path, data_folder: Path ) :
    script_path = Path( "/home/nbellec/research/thesis/DFI" ) / "scripts/execution_sep.sh"
    data_folder.mkdir( exist_ok=True, parents=True )
    disassembly_file = data_folder / "disassembly.txt"
    pc_trace_file = data_folder / "pc_trace.txt"

    if pc_trace_file.exists() and disassembly_file.exists() :
        return disassembly_file, pc_trace_file

    cmd = f'{str( script_path )} {str( binary_path )} ' + \
          f'{str( disassembly_file )} {str( pc_trace_file )}'

    p = sp.run( cmd, shell=True, stderr=sp.PIPE, stdout=sp.PIPE )

    return disassembly_file, pc_trace_file


def recover_dfi_parts( binary_path: Path ) -> Tuple[ List[ DFILoad ], List[ DFIStore ] ] :
    objdump_path = Path( "/opt/riscv32i/bin/riscv32-unknown-elf-objdump" )
    objdump_template = Template( "$objdump -x $exe | grep -E '.dfi_(load|store)' | " +
                                 "awk '{ print \"0x\" $$1 \" : \" $$5 }'" )
    objdump_cmd = objdump_template.substitute( objdump=str( objdump_path ),
                                               exe=str( binary_path ) )

    load_extraction_re = re.compile( r"^(0x[0-9a-f]*) : .dfi_load_(start|tag|check|end)", re.MULTILINE )
    store_extraction_re = re.compile( r"^(0x[0-9a-f]*) : .dfi_store_(sandboxing|kernel|tag|end)", re.MULTILINE )

    p = sp.run( objdump_cmd, shell=True, stdout=sp.PIPE, text=True, encoding='utf-8' )

    load_labels = load_extraction_re.findall( p.stdout )
    store_labels = store_extraction_re.findall( p.stdout )
    dfi_loads = [ ]
    dfi_stores = [ ]

    for selected in select_range( lambda t : t[ 1 ] == "start", lambda t : t[ 1 ] == "end", load_labels ) :
        check_adds = set()

        if selected[ 0 ][ 1 ] != "start" or selected[ 1 ][ 1 ] != "tag" or \
                selected[ -1 ][ 1 ] != "end" :
            raise Exception( "" )

        for s in selected:
            if s[1] == "check":
                check_adds.add( int(s[0], 16) )

        dfi_loads.append(
            DFILoad( kernel_add=int( selected[ 0 ][ 0 ], 0 ), load_add=int( selected[ 1 ][ 0 ], 0 ),
                     check_add=int( selected[ 2 ][ 0 ], 0 ), end_add=int( selected[ -1 ][ 0 ], 0 ), check_tag_start_add=check_adds ) )

    for selected in select_range( lambda t : t[ 1 ] in { "sandboxing", "kernel" }, lambda t : t[ 1 ] == "end",
                                  store_labels ) :
        if selected[ 0 ][ 1 ] == "sandboxing" :
            if selected[ 1 ][ 1 ] != "kernel" or selected[ 2 ][ 1 ] != "tag" or selected[ -1 ][ 1 ] != "end" :
                raise Exception( "" )

            dfi_stores.append(
                DFIStore( sandbox_add=int( selected[ 0 ][ 0 ], 0 ), kernel_add=int( selected[ 1 ][ 0 ], 0 ),
                          store_add=int( selected[ 2 ][ 0 ], 0 ), end_add=int( selected[ -1 ][ 0 ], 0 ) ) )

        elif selected[ 0 ][ 1 ] == "kernel" :
            if selected[ 1 ][ 1 ] != "tag" or selected[ -1 ][ 1 ] != "end" :
                raise Exception( "" )

            dfi_stores.append(
                DFIStore( sandbox_add=None, kernel_add=int( selected[ 0 ][ 0 ], 0 ),
                          store_add=int( selected[ 1 ][ 0 ], 0 ), end_add=int( selected[ -1 ][ 0 ], 0 ) ) )

        else :
            raise Exception( "" )

    return dfi_loads, dfi_stores


def handle_file( filepath: Path ) -> Generator[ str, None, None ] :
    f = open( filepath, "r" )

    for line in f :
        yield line

    f.close()


def qemu_pc_trace( filepath: Path ) -> Iterator[ int ] :
    return map( lambda line : int( line, base=16 ), handle_file( filepath ) )


def qemu_disassembly( filepath: Path ) -> List[ AsmInst ] :
    asm_insts = []
    asm_re = re.compile( "([0-9a-f]*) : ([a-z]*) ([a-z0-9,()-]*)" )

    with open( filepath, "r" ) as f :
        for line in f.readlines() :
            m = asm_re.match( line )
            add = int( m.group( 1 ), base=16 )
            inst = m.group( 2 )
            operands = m.group( 3 )
            asm_insts.append( AsmInst( add, inst, operands ) )

    return asm_insts


cost_inst = dict( [
                      (inst, (1, False)) for inst in [ "add", "addi", "and", "andi", "auipc", "lui", "or", "ori",
                                                       "sll", "slli", "slt", "slti", "sltiu", "sltu", "sra", "srai",
                                                       "srl", "srli", "sub", "xor", "xori" ]
                  ] + [
                      (inst, (2, False)) for inst in [ "lb", "lbu", "lh", "lhu", "lw", "sb", "sh", "sw" ]
                  ] + [
                      (inst, (2, True)) for inst in
                      [ "beq", "bge", "bgeu", "blt", "bltu", "bne", "jal", "jalr", "ebreak" ]
                  ] + [
                      (inst, (34, False)) for inst in [ "div", "divu", "mul", "mulh", "mulhu", "rem", "remu" ]
                  ] )

def compute_cost_instruction( inst: str ) -> Tuple[ int, bool ] :
    global cost_inst
    return cost_inst[ inst ]

def handle_load( current_load, step: int, asm, ex_cost ) :
    inst = asm[ step ].instruction
    cost, is_branch = compute_cost_instruction( inst )

    ty: CostType
    if step < current_load.load_add :
        ex_cost.load_kernel += cost
        ty = CostType.LOAD_KERNEL
    elif step < current_load.check_add :
        ex_cost.load_exec += cost
        ty = CostType.LOAD_EXEC
    elif step < current_load.end_add :
        if step in current_load.check_tag_start_add:
            ex_cost.check_nb += 1

        ex_cost.load_checks += cost
        ty = CostType.LOAD_CHECKS
    else :
        raise Exception( "" )

    if is_branch :
        ex_cost.store_branch_data( step, ty )


def handle_store( current_store, step, asm, ex_cost ) :
    inst = asm[ step ].instruction
    cost, is_branch = compute_cost_instruction( inst )

    ty: CostType
    if step < current_store.kernel_add :
        ex_cost.store_sandbox += cost
        ty = CostType.STORE_SANDBOX
    elif step < current_store.store_add :
        ex_cost.store_kernel += cost
        ty = CostType.STORE_KERNEL
    elif step < current_store.end_add :
        ex_cost.store_exec += cost
        ty = CostType.STORE_EXEC
    else :
        raise Exception( "" )

    if is_branch :
        ex_cost.store_branch_data( step, ty )


def compute_cost_dfi( dfi_loads: List[ DFILoad ], dfi_stores: List[ DFIStore ],
                      pc_trace: Iterator[ int ],
                      asm: Dict[ int, AsmInst ] ) -> ExecutionStats :
    dfi_start_load_map = { load.kernel_add : load for load in dfi_loads }
    dfi_start_store_map = { s.sandbox_add if s.sandbox_add is not None else s.kernel_add : s for s in dfi_stores }

    ex_cost = ExecutionStats()

    current_load: Optional[ DFILoad ] = None
    current_store: Optional[ DFIStore ] = None
    for step in pc_trace :
        ex_cost.resolve_branch( step )

        if step in dfi_start_load_map :
            current_load = dfi_start_load_map[ step ]
            ex_cost.load_nb += 1
        if step in dfi_start_store_map :
            current_store = dfi_start_store_map[ step ]
            ex_cost.store_nb += 1

        if current_store is not None :
            if step >= current_store.end_add :
                current_store = None
            else :
                handle_store( current_store, step, asm, ex_cost )
        if current_load is not None :
            if step >= current_load.end_add :
                current_load = None
            else :
                handle_load( current_load, step, asm, ex_cost )

    return ex_cost


interesting_bench = [
    "tacle_lift",
    "tacle_powerwindow",
    "tacle_adpcm_dec",
    "tacle_petrinet",
    "tacle_ndes",
    "tacle_mpeg2",
    "tacle_cjpeg_transupp",
    "tacle_g723_enc",
    "tacle_audiobeam",
    "tacle_cjpeg_wrbmp",
    "tacle_dijkstra",
    "tacle_fmref",
    "tacle_adpcm_enc",
    "tacle_h264_dec",
    "tacle_epic",
    "tacle_gsm_dec",
    "tacle_huff_dec",
    "tacle_statemate",
    "tacle_gsm_enc",
    "tacle_cover",
    "tacle_test3",
    "tacle_duff",
    "tacle_cubic",
    "tacle_cosf",
    "tacle_pm",
    "tacle_fft",
    "tacle_rad2deg",
    "tacle_minver",
    "tacle_lms",
    "tacle_st",
    "tacle_ludcmp",
    "tacle_md5",
    "tacle_filterbank",
    "tacle_countnegative",
    "tacle_insertsort",
    "tacle_fir2dim",
    "tacle_matrix1",
    "tacle_binarysearch",
    "tacle_jfdctint",
    "tacle_bsort",
    "tacle_sha",
    "tacle_deg2rad",
    "tacle_isqrt",
    "tacle_iir",
    "tacle_prime",
    "tacle_complex_updates",
]

base = Path( "/home/nbellec/research/thesis/DFI" )
global_data = base / "traces/dfi_overhead.csv"

if __name__ == "__main__" :
    ( base / "traces" ).mkdir(exist_ok=True, parents=True)
    with open( global_data, "w" ) as f:
        f.write( "BENCH,LOAD_NB,STORE_NB,CHECK_NB,MEAN_TAG_CHECK,"
                 "LOAD_KERNEL,LOAD_EXEC,LOAD_CHECK,STORE_SANDBOX,STORE_KERNEL,STORE_EXEC,"
                 "PERCENT_LOAD_KERNEL,PERCENT_LOAD_EXEC,PERCENT_LOAD_CHECKS,PERCENT_STORE_SANDBOX,PERCENT_STORE_KERNEL,PERCENT_STORE_EXEC,TOTAL\n" )

    for bench_name in interesting_bench :
        executable_path = base / f"tests/build/bin/{bench_name}/{bench_name}.elf.dfi.comet"
        data_folder = base / f"traces/{bench_name}"
        dfi_loads, dfi_stores = recover_dfi_parts( executable_path )
        print( f"{bench_name.upper():^40}" )
        print( "Execute gen_trace" )
        disassembly_file, pc_trace_file = gen_trace( executable_path, data_folder )

        print( "Analyze data" )
        asm = qemu_disassembly( disassembly_file )
        asm_dict = { x.address : x for x in asm }
        pc_trace = qemu_pc_trace( pc_trace_file )
        dfi_cost = compute_cost_dfi( dfi_loads, dfi_stores, pc_trace, asm_dict )

        total_cost = dfi_cost.total_cost

        with open( data_folder / "dfi_breakout.txt", "w" ) as f :
            f.write( f"load_nb       : {dfi_cost.load_nb:>15,}\n")
            f.write( f"store_nb      : {dfi_cost.store_nb:>15,}\n")
            f.write( f"check_nb      : {dfi_cost.check_nb:>15,} ({dfi_cost.check_nb / dfi_cost.load_nb:>6.2})\n")
            f.write( f"load_kernel   : {dfi_cost.load_kernel:>15,} ({dfi_cost.load_kernel / total_cost:>6.2%})\n" )
            f.write( f"load_exec     : {dfi_cost.load_exec:>15,} ({dfi_cost.load_exec / total_cost:>6.2%})\n" )
            f.write( f"load_checks   : {dfi_cost.load_checks:>15,} ({dfi_cost.load_checks / total_cost:>6.2%})\n" )
            f.write( f"store_sandbox : {dfi_cost.store_sandbox:>15,} ({dfi_cost.store_sandbox / total_cost:>6.2%})\n" )
            f.write( f"store_kernel  : {dfi_cost.store_kernel:>15,} ({dfi_cost.store_kernel / total_cost:>6.2%})\n" )
            f.write( f"store_exec    : {dfi_cost.store_exec:>15,} ({dfi_cost.store_exec / total_cost:>6.2%})\n" )

        with open( global_data, "a" ) as f :
            f.write( f"{bench_name},{dfi_cost.load_nb},{dfi_cost.store_nb},{dfi_cost.check_nb},{dfi_cost.check_nb / dfi_cost.load_nb},")
            f.write( f"{dfi_cost.load_kernel},{dfi_cost.load_exec},{dfi_cost.load_checks},")
            f.write( f"{dfi_cost.store_sandbox},{dfi_cost.store_kernel},{dfi_cost.store_exec},")
            f.write( f"{dfi_cost.load_kernel/total_cost},{dfi_cost.load_exec/total_cost},{dfi_cost.load_checks/total_cost},")
            f.write( f"{dfi_cost.store_sandbox/total_cost},{dfi_cost.store_kernel/total_cost},{dfi_cost.store_exec/total_cost},")
            f.write( f"{total_cost}\n" )

        print( f"load_nb       : {dfi_cost.load_nb:>15,}\n" )
        print( f"store_nb      : {dfi_cost.store_nb:>15,}\n" )
        print( f"check_nb      : {dfi_cost.check_nb:>15,} ({dfi_cost.check_nb / dfi_cost.load_nb:>6.2})\n" )
        print( f"load_kernel   : {dfi_cost.load_kernel:>15,} ({dfi_cost.load_kernel / total_cost:>6.2%})\n" )
        print( f"load_exec     : {dfi_cost.load_exec:>15,} ({dfi_cost.load_exec / total_cost:>6.2%})\n" )
        print( f"load_checks   : {dfi_cost.load_checks:>15,} ({dfi_cost.load_checks / total_cost:>6.2%})\n" )
        print( f"store_sandbox : {dfi_cost.store_sandbox:>15,} ({dfi_cost.store_sandbox / total_cost:>6.2%})\n" )
        print( f"store_kernel  : {dfi_cost.store_kernel:>15,} ({dfi_cost.store_kernel / total_cost:>6.2%})\n" )
        print( f"store_exec    : {dfi_cost.store_exec:>15,} ({dfi_cost.store_exec / total_cost:>6.2%})\n" )