/*
  See https://wiki.osdev.org/RISC-V_Meaty_Skeleton_with_QEMU_virt_board
  for more information on what is happening in this file :)
*/
#include <stdint.h>

#define UART_ADDR (uint8_t*) 0x10000000
#define SYSCON_ADDR (uint32_t *) 0x100000

void init_uart() {
  volatile uint8_t* uart = UART_ADDR;
  *(uart + 3) = 0x3;
  *(uart + 2) = 0x1;
}

void put(char c) {
  volatile char* uart = UART_ADDR;
  *uart = c;
}

void put_s(char* c) {
  volatile char* uart = UART_ADDR;
  for (unsigned int i = 0; c[i] != '\0'; i++) {
    *uart = c[i];
  }
}

void print_int(unsigned int integer) {
  volatile char* uart = UART_ADDR;
  char* tab_int = (char*) &integer;
  char c = 0;
  char toprint = 0;
  put_s( "0x" );
  if (integer == 0) {
    put('0');
  } else {
    for (int i = 3; i >= 0; --i) {
      c = (char) tab_int[i] >> 4;
      if ( c != 0 ) {
        toprint = 1;
      }
      if ( toprint ) {
        if ( c < 10 ) {
          *uart = '0' + c;
        } else {
          *uart = 'a' + c - 10;
        }
      }
      c = (char) tab_int[i] & 0b1111;
      if ( c != 0 ) {
        toprint = 1;
      }
      if ( toprint ) {
        if ( c < 10 ) {
          *uart = '0' + c;
        } else {
          *uart = 'a' + c - 10;
        }
      }
    }
  } // integer != 0
}

void poweroff(void) {
  volatile uint32_t* syscom = SYSCON_ADDR;
  *syscom = 0x5555;
}

void print_function_entry( char* fname ) {
  init_uart();
  put_s("Entry function : ");
  put_s( fname );
  put( '\n' );
}

void print_function_exit( char* fname ) {
  put_s("Exit function : ");
  put_s( fname );
  put( '\n' );
}

void print_loop_entry( char* fname, int ltag ) {
  put_s("Entry loop : ");
  put_s( fname );
  put_s( " ! " );
  print_int( ltag );
  put( '\n' );
}

void print_loop_exit( char* fname, int ltag ) {
  put_s("Exit loop : ");
  put_s( fname );
  put_s( " ! " );
  print_int( ltag );
  put( '\n' );
}

void handle_interrupt(uint32_t mepc) {
  put_s( "FAILED : " );
  print_int( mepc );
  put('\n');
  poweroff();
}

void ok_end(int main_return_val) {
  if ( main_return_val != 0 ) {
    put_s("WARNING : main - ");
    print_int( main_return_val );
    put('\n');
  }
  put_s( "OK\n");
  poweroff();
}

// void *memcpy(void *dest, const void *src, unsigned long int n) {
//   for (unsigned int i = 0; i < n; ++i) {
//     ((char*)dest)[i] = ((char*)src)[i];
//   }
//   return dest;
// }
