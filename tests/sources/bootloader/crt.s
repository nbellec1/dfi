.section ".text.crt"
.globl _start
_start:
      # set stack pointer
      # write to a2 first, to guarantee an atomic write to sp
      # li      a2, 0x80007ff0
      lui     a2, %hi(_stack_up)
      addi    a2, a2, %lo(_stack_up)
      mv      sp, a2
      # Prepare the mtvec register
      la      a2, _trap_vec
      csrw    mtvec, a2
      # Execute the main program
      jal     main
      # Write the ending string
      call    ok_end
# 1:    j       1b

.section ".trap.vec"
.globl _trap_vec
_trap_vec:
  csrr  a0, mepc
  call  handle_interrupt
