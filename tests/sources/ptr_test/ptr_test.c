int swap(int* i, int j) {
  int k = *i;
  *i = j;

  return k;
}

int main() {
  int i,j;
  i = 5;
  j = 8;

  j = swap(&i,j);
  i = swap(&j,i);

  return j+i;
}
