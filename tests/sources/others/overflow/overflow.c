#include "lib.h"

int overflow(unsigned int* data) {
  put_s("OVERFLOW\n");
  for (unsigned int i = 0; i <= 3; i++) {
    print_int( data[ i ] );
    put_s("\n");
  }
  return 1;
}

int normalflow(unsigned int* data) {
  put_s("NORMAL\n");
  for (unsigned int i = 0; i <= 3; i++) {
    print_int( data[ i ] );
    put_s("\n");
  }
  return 0;
}

void copy(unsigned int** data) {
  for ( int i = 0; i <= 4; i++ ) {
    data[ i ] = (unsigned int *) &overflow;
  }
}

int main() {
  int (*func_ptr)(unsigned int*) = &normalflow;
  unsigned int data[ 4 ];

  copy( (unsigned int**) &data );

  int ret = (*func_ptr)( data );

  return ret;
}
