#!/bin/bash

function make_ais() {
  executable=$1
  initial_ais=$2
  ais=$3

  cp ${initial_ais} ${ais}

  riscv32-unknown-elf-objdump -d ${executable} | \
    grep "ebreak" | \
    awk -F: '{print "instruction 0x" $1 " infeasible;"}' >> ${ais}

  riscv32-unknown-elf-objdump -x ${executable} | \
    grep ".bsdfi_" | \
    awk '{print "routine \"main\" snippet \"" $5 "\" to \"end_" $5 "\" evaluate as : \"name" $5 "\";" }' | \
    sed s/"end_.bsdfi"/".esdfi"/g | \
    sed s/"name.bsdfi_"/"sdfi_"/g >> ${ais}

  riscv32-unknown-elf-objdump -x ${executable} | \
    grep ".bldfi_" | \
    awk '{print "routine \"main\" snippet \"" $5 "\" to \"end_" $5 "\" evaluate as : \"name" $5 "\";" }' | \
    sed s/"end_.bldfi"/".eldfi"/g | \
    sed s/"name.bldfi_"/"ldfi_"/g >> ${ais}

  riscv32-unknown-elf-objdump -x ${executable} | \
    grep ".bcdfi_" | \
    awk '{print "routine \"main\" snippet \"" $5 "\" to \"end_" $5 "\" evaluate as : \"name" $5 "\";" }' | \
    sed s/"end_.bcdfi"/".ecdfi"/g | \
    sed s/"name.bcdfi_"/"cdfi_"/g >> ${ais}

  riscv32-unknown-elf-objdump -x ${executable} | \
    grep ".bbdfi_" | \
    awk '{print "routine \"main\" snippet \"" $5 "\" to \"end_" $5 "\" evaluate as : \"name" $5 "\";" }' | \
    sed s/"end_.bbdfi"/".ebdfi"/g | \
    sed s/"name.bbdfi_"/"bdfi_"/g >> ${ais}
}

function make_apx() {
  executable=$1
  ais=$2
  apx=$3
  xml_report=$4

  cp "templates/template_dfi.apx" ${apx}
  sed -i s[{xml_report}[${xml_report}[g ${apx}
  sed -i s[{executable}[${executable}[g ${apx}
  sed -i s[{ais}[${ais}[g ${apx}
}

function get_wcet() {
  apx=$1
  report=$2
  name=$3

  alauncher -b $apx
  r=`grep "<wcet>" $2 | sed s/"<wcet>"//g | sed s["cycles</wcet></wcet_analysis>"[[g`
  echo "$r"
}
