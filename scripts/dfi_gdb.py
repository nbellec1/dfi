# coding=utf-8
import struct
import gdb
import pprint
import re
import sys
from enum import IntEnum, auto
from typing import List, Dict
from collections import namedtuple

LOG_FILE = "/tmp/gdb.log"
FILENAME = str( gdb.current_progspace().filename )

def get_register_data( reg: str ) -> int :
    frame = gdb.newest_frame()
    val = frame.read_register( reg )
    val = str( val ).split(' ')[0]
    if val[:2] == '0x':
        return int(val, base=16)
    else:
        return struct.unpack('>L', struct.pack('>l', int(val)))[0]


def get_label_add( label: str ) :
    data = gdb.execute( f"info address {label}", to_string=True )
    return re.match( "Symbol \"[._a-zA-Z0-9]*\" is at (0x[0-9a-f]*)", data ).group( 1 )

class DFIComponent( IntEnum ) :
    BEGIN_SANDBOXING: int = auto()
    STORE_KERNEL: int = auto()
    BEGIN_STORE_TAG: int = auto()
    END_STORE_TAG: int = auto()
    LOAD_START: int = auto()
    BEGIN_LOAD_TAG: int = auto()
    BEGIN_CHECK_TAG: int = auto()
    END_CHECK_TAG: int = auto()

    @staticmethod
    def ty_from_label( label: str ) -> int :
        if "dfi_store_sandboxing" in label :
            return DFIComponent.BEGIN_SANDBOXING
        elif "dfi_store_kernel" in label :
            return DFIComponent.STORE_KERNEL
        elif "dfi_store_tag" in label :
            return DFIComponent.BEGIN_STORE_TAG
        elif "dfi_store_end" in label :
            return DFIComponent.END_STORE_TAG
        elif "dfi_load_start" in label:
            return DFIComponent.LOAD_START
        elif "dfi_load_tag" in label :
            return DFIComponent.BEGIN_LOAD_TAG
        elif "dfi_load_check" in label :
            return DFIComponent.BEGIN_CHECK_TAG
        elif "dfi_load_end" in label :
            return DFIComponent.END_CHECK_TAG
        else :
            raise Exception( "Failed to analyze label: {label}".format( label=label ) )

class DFIStackElement( object ) :
    def __init__( self, label ) :
        self.label: str = label
        self.finished: bool = False
        self.data = { }

    def get_style( self ) :
        ty = DFIComponent.ty_from_label( self.label )
        if ty == DFIComponent.BEGIN_SANDBOXING :
            # Bold Green label
            return "1;32m"

        if ty == DFIComponent.BEGIN_STORE_TAG or ty == DFIComponent.END_STORE_TAG :
            # Bold Magenta label
            return "1;35m"

        if ty == DFIComponent.BEGIN_LOAD_TAG :
            # Bold Cyan label
            return "1;36m"

        if ty == DFIComponent.BEGIN_CHECK_TAG or ty == DFIComponent.END_CHECK_TAG:
            # Bold Red label
            return "1;31m"

    def __repr__( self ) :
        style = self.get_style()
        done = "DONE" if self.finished else ""
        return "\033[{style}{label}\t{done}\t{data}\033[0m".format( style=style, label=self.label, done=done,
                                                                    data=str( self.data ) )

    def __str__( self ) :
        return self.__repr__()


dfi_stack: List[ DFIStackElement ] = [ ]
dfi_precomputed = 0x44001000
dfi_time: int = 0
dfi_branch_add = None


class DFIBP( gdb.Breakpoint ) :
    def __init__( self, spec, label ) :
        super().__init__( spec, gdb.BP_BREAKPOINT )
        self.label = label


class EbreakBP( DFIBP ) :
    """ Breakpoint specific to EBREAK instructions """
    def __init__( self, spec ) :
        super().__init__( spec, None )

    def stop( self ) :
        return True


class DFIStoreBP( DFIBP ) :
    """Breakpoint at the moment we execute a non-dfi store"""
    def __init__( self, spec, label ) :
        super().__init__( spec, label )

    def stop( self ) :
        global dfi_stack, dfi_precomputed

        arch=gdb.selected_frame().architecture()

        store_re = re.compile( "(?:sbu|shu|sw|sb|sh)\t([a-z0-9]{2,3}|zero),(-?[0-9]*)\(([a-z0-9]{2,3})\)" )
        # Compute the register and index of the store
        disassembled_code = arch.disassemble( get_register_data( "pc" ) )[0]["asm"]
        store_inst = store_re.search( disassembled_code )

        if store_inst is None:
            print( disassembled_code )
            raise Exception( "Unable to find the store address" )

        store_add = get_register_data( store_inst.group(3) ) + int( store_inst.group(2), base=10 )
        dfi_store_add = ( store_add // 4 ) * 2 + dfi_precomputed
        tag = get_register_data( "t4" )

        el = DFIStackElement( self.label )
        el.data[ "store_add" ] = hex( store_add )
        el.data[ "dfi_rdt_add" ] = hex( dfi_store_add )
        el.data[ "tag" ] = tag

        dfi_stack.append( el )

        # Continue the program
        return False


class DFILoadBP( DFIBP ) :
    """
Breakpoint representing a load in the program, protected by DFI
    """
    def __init__( self, spec, label ) :
        super().__init__( spec, label )

    def stop( self ) :
        global dfi_stack, dfi_precomputed

        arch=gdb.selected_frame().architecture()

        load_re = re.compile( "(?:lbu|lhu|lw|lb|lh)\t([a-z0-9]{2,3}|zero),(-?[0-9]*)\(([a-z0-9]{2,3})\)" )
        # Compute the register and index of the store
        disassembled_code = arch.disassemble( get_register_data( "pc" ) )[0]["asm"]

        load_inst = load_re.search( disassembled_code )

        if load_inst is None:
            print( disassembled_code )
            raise Exception( "Unable to find the load address" )

        load_add = get_register_data( load_inst.group(3) ) + int( load_inst.group(2), base=10 )
        dfi_load_add = ( load_add // 4 ) * 2 + dfi_precomputed
        last_tag_checked = get_register_data( "t3" )

        el = DFIStackElement( self.label )
        el.data[ "load_add" ] = hex( load_add )
        el.data[ "dfi_rdt_add" ] = hex( dfi_load_add )
        el.data[ "last_tag" ] = last_tag_checked
        dfi_stack.append( el )

        # Continue the program
        return False

class DFITimeBP( gdb.Breakpoint ):
    """
Breakpoint used to recover the execution time of the programs
    """
    def __init__( self, add, inst_type ):
        super().__init__( "*{add}".format(add=add), gdb.BP_BREAKPOINT )
        self.inst_type = inst_type
        self.add = int(add, base=16)

    def stop( self ):
        global dfi_time, dfi_branch_add

        if dfi_branch_add is not None:
            if dfi_branch_add != self.add:
                dfi_time += 1
            dfi_branch_add = None

        if "mul" in self.inst_type:
            dfi_time += 34
        elif "div" in self.inst_type:
            dfi_time += 34
        elif "rem" in self.inst_type:
            dfi_time += 34
        elif "fence" in self.inst_type:
            dfi_time += 3
        elif self.inst_type in {"jal", "j", "ebreak", "ecall", "fence"}:
            dfi_time += 3
        elif "cs" in self.inst_type:
            dfi_time += 2
        elif self.inst_type in {"sb", "sh", "sw", "lb", "lh", "lw", "lbu", "lhu"}:
            dfi_time += 2
        elif "sl" in self.inst_type:
            dfi_time += 1
        elif "sr" in self.inst_type:
            dfi_time += 1
        elif "add" in self.inst_type:
            dfi_time += 1
        elif "sub" in self.inst_type:
            dfi_time += 1
        elif "slt" in self.inst_type:
            dfi_time += 1
        elif "xor" in self.inst_type:
            dfi_time += 1
        elif "or" in self.inst_type:
            dfi_time += 1
        elif "and" in self.inst_type:
            dfi_time += 1
        elif self.inst_type in {"nop"}:
            dfi_time += 1
        elif self.inst_type in {"lui", "li", "auipc", "mv"}:
            dfi_time += 1
        elif self.inst_type in {"beq", "beqz", "bnez", "bne", "blt", "blez", "bge", "bgez", "bgtz", "bltz", "bltu", "bgeu", "ret"}:
            dfi_time += 2
            dfi_branch_add = self.add + 0x4
        else:
            print("Instruction not found : " + str(self.inst_type))
            return True

        return False

class SelectedWatchPoint( gdb.Breakpoint ):
    def __init__( self, cmd, ignore_list, switch = False ):
        super().__init__( cmd, gdb.BP_WATCHPOINT, wp_class=gdb.WP_ACCESS )
        self.ignore = [ add for add in ignore_list ]
        self.switch = switch

    def stop( self ):
        data = get_register_data( "pc" )
        # self.last_add = data
        print( hex(data) )
        if data in self.ignore:
            return self.switch

        return not self.switch


class DFICommand( gdb.Command ) :
    """ General class for the command about the DFI  """

    def __init__( self ) :
        super().__init__( "dfi", gdb.COMMAND_USER, prefix=True )

    def invoke( self, args, tty ) :
        pass

class DumpDFITable( gdb.Command ) :
    """Dump DFI Table on the console."""

    def __init__( self ) :
        super().__init__( "dfi table", gdb.COMMAND_USER )

    def invoke( self, args, tty ) :
        print( "Dump DFI Table" )

class DFIStackCommand( gdb.Command ) :
    """Dump DFI Stack on the console."""

    def __init__( self ) :
        super().__init__( "dfi stack", gdb.COMMAND_USER, prefix=True )

    def invoke( self, args, tty ) :
        global dfi_stack
        print( "DFI Stack" )
        for i, s in enumerate( dfi_stack ) :
            print( "[{i}]\t{el}".format(i=i, el=str( dfi_stack[i] ) ) )

class DFIStackPrintCommand( gdb.Command ) :
    """Dump DFI Stack on the console."""

    def __init__( self ) :
        super().__init__( "dfi stack print", gdb.COMMAND_USER )

    def invoke( self, args, tty ) :
        global dfi_stack

        argv = gdb.string_to_argv( args )
        last = int( argv[ 0 ] ) if len( argv ) >= 1 else None

        print( "DFI Stack" )
        for i in range(len(dfi_stack)) :
            if last is None or i == last:
                print( "[{i}]\t{el}".format(i=i, el=str( dfi_stack[i] ) ) )

class DFIStackLastCommand( gdb.Command ) :
    """Dump DFI Stack on the console."""

    def __init__( self ) :
        super().__init__( "dfi stack last", gdb.COMMAND_USER )

    def invoke( self, args, tty ) :
        global dfi_stack

        argv = gdb.string_to_argv( args )
        last = int( argv[ 0 ], base=16 ) if len(argv) >= 1 else 1

        print( "DFI Stack" )
        # Can be optimized by a reversed order iteration
        for i in range( len(dfi_stack) ) :
            if i >= len(dfi_stack) - last:
                print( "[{i}]\t{el}".format(i=i, el=str( dfi_stack[i] ) ) )

class DFIStackSearchCommand( gdb.Command ):
    def __init__(self):
        super().__init__( "dfi stack search", gdb.COMMAND_USER )

    def invoke( self, args, tty ) :
        global dfi_stack

        argv = gdb.string_to_argv( args )
        add = int(argv[0], base=16)
        offset = int(argv[1], base=16) - 1 if len(argv) >= 2 else 1

        low_add = add - offset
        up_add = add + offset
        for i, stack_el in enumerate(dfi_stack):
            for d in stack_el.data:
                if "add" in d and low_add <= int(stack_el.data[d], base=16) <= up_add:
                    print( "[{i}]\t{el}".format(i=i, el=str( stack_el ) ) )

class DFIStackIndexCommand( gdb.Command ):
    def __init__(self):
        super().__init__( "dfi stack index", gdb.COMMAND_USER )

    def invoke( self, args, tty ) :
        global dfi_stack

        argv = gdb.string_to_argv( args )
        index = int(argv[0])
        offset = int(argv[1]) if len(argv) >= 2 else 1

        low_index = index - offset
        up_index = index + offset
        for i, stack_el in enumerate(dfi_stack):
            if low_index <= i <= up_index:
                print( "[{i}]\t{el}".format(i=i, el=str( stack_el ) ) )

class DFIDebugCommand( gdb.Command ):
    def __init__(self):
        super().__init__("dfi debug", gdb.COMMAND_USER, prefix=True)

    def invoke( self, args, tty ):
        pass

class DFIFastDebugCommand( gdb.Command ):
    def __init__(self):
        super().__init__("dfi debug fast", gdb.COMMAND_USER)

    def invoke( self, args, tty ):
        set_breakpoints( only_ebreaks=True )
        register_handlers()
        gdb.execute( "break *_start+0x10" )
        gdb.execute( "continue" )

class DFIFullDebugCommand( gdb.Command ):
    def __init__(self):
        super().__init__("dfi debug full", gdb.COMMAND_USER)

    def invoke( self, args, tty ):
        set_breakpoints( only_ebreaks=False )
        register_handlers()
        gdb.execute( "break *_start+0x10" )
        gdb.execute( "continue" )

class DFITimeCommand( gdb.Command ):
    def __init__( self ):
        super().__init__("dfi time", gdb.COMMAND_USER)

    def invoke( self, args, tty ):
        gdb.execute( "break *_start + 0xc", from_tty=True )
        gdb.execute( "continue" )
        gdb.execute( "break *_start + 0x10", from_tty=True )
        set_time_bp()
        gdb.execute( "continue" )
        print("\n\033[1;32m"+"Time : "+str(dfi_time)+"\033[0m")

class DFIFindSymbol( gdb.Command ):
    def __init__( self ):
        super().__init__( "dfi sym", gdb.COMMAND_USER )

    def invoke( self, args, tty ):
        argv = gdb.string_to_argv( args )
        add = int( args, base=16 )

        for add_sym, name_sym in get_dfi_labels():
            if int( add_sym, base=16 ) == add:
                print( "Symbol found : " + str( name_sym ) )
                break
        else:
            print( "Unable to find symbol" )


def setup() :
    print( 'Running GDB from: {pythondir}'.format( pythondir=gdb.PYTHONDIR ) )
    print( "Executing : {filename}".format( filename=FILENAME ) )
    gdb.execute( "set pagination off" )
    gdb.execute( "set print pretty" )
    # gdb.execute("set logging file {log_file}".format(log_file=LOG_FILE))
    # gdb.execute("set logging on")
    gdb.execute( "target remote localhost:1234", from_tty=True )
    gdb.execute( "maintenance packet Qqemu.PhyMemMode:1", from_tty=True )
    print( "\nSetup complete !!\n" )

def get_functions() :
    symbol_re = re.compile( "\[ ?[0-9]*\] ([a-zA-Z]) (0x[0-9a-f]*) ([_a-zA-Z.0-9]*) section ([_a-zA-Z.0-9]*)" )
    symbols = gdb.execute( "maintenance print msymbols", from_tty=True, to_string=True ).split( "\n" )

    for sym in symbols:
        m = symbol_re.match( sym )
        if m is None:
            continue

        if m.group(1) not in ["T", "t"]:
            continue

        if "_stack" in m.group(3) or \
           "_text" in m.group(3) or \
           "dfi" in m.group(3):
            continue

        yield m.group(3)

def get_ebreaks_address() :
    add_reg = re.compile( "^ {3}(0x[0-9a-f]*)" )

    for func in get_functions():
        disas = filter(
            lambda inst : "ebreak" in inst,
            gdb.execute( "disassemble {func}".format( func=func ), from_tty=True, to_string=True ).split( "\n" )
        )

        for ebreak in disas :
            yield add_reg.match( ebreak ).group( 1 )


def get_dfi_labels() :
    symbol_re = re.compile( "\[( ?[0-9]*)\] ([a-zA-Z]) (0x[0-9a-f]*) ([_a-zA-Z.0-9]*) section ([_a-zA-Z.0-9]*)" )
    symbols = gdb.execute( "maintenance print msymbols", from_tty=True, to_string=True ).split( "\n" )

    for sym in symbols :
        m = symbol_re.match( sym )
        if m is None :
            continue

        if m.group( 2 ) != "t" :
            continue

        if "dfi" not in m.group( 4 ) :
            continue

        yield m.group(3), m.group(4)

def stop_handler() :
    # Print the disassembled code around the stop
    gdb.execute( "disassemble $pc-0x20,$pc+0x20" )


def ebreak_handler( bp ) :
    stop_handler()
    gdb.execute( "info registers t3 t4" )
    gdb.execute( "dfi stack last 10" )

def bp_handler( event ) :
    if type( event ) == gdb.BreakpointEvent:
        for bp in event.breakpoints:
            if type( bp ) == EbreakBP:
                ebreak_handler( bp )
                break
        else:
            stop_handler()


def register_handlers() :
    gdb.events.stop.connect( bp_handler )
    print( "Done setting event handlers" )


def set_breakpoints(only_ebreaks = False) :
    for add in get_ebreaks_address() :
        EbreakBP( "*{add}".format( add=add ) )

    if not only_ebreaks:
        for add, label in get_dfi_labels() :
            ty = DFIComponent.ty_from_label( label )
            if ty == DFIComponent.END_STORE_TAG:
                DFIStoreBP( "*{add}".format( add=add ), label )
            elif ty == DFIComponent.END_CHECK_TAG:
                DFILoadBP( "*{add}".format( add=add ), label )

def set_time_bp():
    for func in get_functions():
        print(func)
        add_reg = re.compile( "^ {3}(0x[0-9a-f]*) <\+[0-9]+>:\t([a-z]+)" )
        dis = gdb.execute( f"disassemble {func}", from_tty=True, to_string=True ).split( "\n" )

        for inst in dis:
            m = add_reg.match( inst )
            if m is not None:
                add, ins = m.group( 1 ), m.group( 2 )
                DFITimeBP( add, ins )

def main() :
    setup()
    # Special command for debugging one programm
    # gdb.execute( "break *0x80000000" )
    # gdb.execute( "c" )
    # while True:
    #     pc = get_register_data('pc')
    #     print(f"{pc}")
    #     if pc == 0x8000001c:
    #         break

    #     gdb.execute( "stepi" )
    # gdb.execute( "break handle_interrupt" )
    # gdb.execute( "dfi debug full" )
    # gdb.execute( "c" )
    # gdb.execute( "awatch *(unsigned short *)0x83000fcc" )
    # SelectedWatchPoint( "*(unsigned short *)0x83000fcc", [0x800031b0], True )

class PCStep( gdb.Command ) :
    def __init__( self ) :
        super().__init__( "pcstepi", gdb.COMMAND_USER )

    def invoke( self, args, tty ):
        gdb.execute( "b *0x80000000" )
        gdb.execute( "c" )

        filename = args if len(args) > 0 else "trace.txt" 
        
        with open(filename, "w") as file:
            for i in range(261874):
                pc = get_register_data('pc')
                file.write(f"{hex(pc-0x80000004)}\n")
                if pc == 0x8000001c:
                    break

                gdb.execute( "stepi" )

class Advance( gdb.Command ) :
    def __init__( self ) :
        super().__init__( "advance", gdb.COMMAND_USER )

    def invoke( self, args, tty ):
        gdb.execute( "b *0x80000000" )
        gdb.execute( "c" )

        gdb.execute( "watch -l *(int*)0x80002400" )
        gdb.execute( "b *0x800002b0" )
        gdb.execute( "c" )

DFICommand()
DFIStackCommand()
DFIStackPrintCommand()
DFIStackSearchCommand()
DFIStackLastCommand()
DFIStackIndexCommand()
DFIDebugCommand()
DFIFastDebugCommand()
DFIFullDebugCommand()
DFITimeCommand()
DFIFindSymbol()
PCStep()
Advance()

main()
