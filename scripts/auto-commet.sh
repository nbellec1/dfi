source scripts/benchmarks.sh

file="commet-result.txt"
file_tmp=".commet-result.tmp"

rm -f ${file}
rm -f "${file_tmp}"

echo "BENCH : CC | DFI" >> ${file_tmp}

commet="/home/nbellec/research/thesis/Comet/build/bin/comet.sim"
output_dir="./commet-output"

mkdir -p ${output_dir}

for name in ${tests[@]}
do
  time_dfi=`${commet} -f "./tests/build/bin/dfi/${name}.elf" | tee "${output_dir}/${name}_dfi-output.txt" | grep -A 1 "KILL" | tail -n 1`
  time_cc=`${commet} -f "./tests/build/bin/control_case/${name}.elf" | tee "${output_dir}/${name}_cc-output.txt" | grep -A 1 "KILL" | tail -n 1`

  echo "${name} : ${time_cc} | ${time_dfi}" >> ${file_tmp}

  status=$?
done

column -t -s \|: ${file_tmp} > ${file}

rm -f "${file_tmp}"
